#include <iostream>
#include <vector>
#include <string>
using namespace std;
class Animal
{
public:

        virtual void Voice() = 0;

        virtual ~Animal()
        {

        }
    
};
class Dog : public Animal {

    void Voice() override
    {
        cout << "Woof!" << endl;

    }
};
class Cat : public Animal {

    void Voice() override
    {
        cout << "Meow!" << endl;

    }
};
class Snake : public Animal {

    void Voice() override
    {
        cout << "Shhhh!" << endl;

    }
};


int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Snake();

    for (Animal* voice : animals){
        voice->Voice();
        delete voice;

    }
  



        


    return 0;
}